// Lab - Standard Template Library - Part 4 - Stacks
// FIRSTNAME, LASTNAME

#include <iostream>
#include <string>
#include <stack>
using namespace std;

int main()
{
	stack<string> letters;
	bool done = false;
	while (!done) {
		string word;
		cout << "Enter the next letter of the word. Enter UNDO to remove the last entry or DONE to be finished. ";
		cin >> word;
		if (word == "UNDO") {
			cout << "removed " << letters.top()<<endl;
			letters.pop();
			
		}
		else if (word == "DONE") {
			done = true;
		}
		else
			letters.push(word);
	}
	cout << "Finished word: ";
	while(!letters.empty()) {
		cout << letters.top();
		letters.pop();
	}
    cin.ignore();
    cin.get();
    return 0;
}
