// Lab - Standard Template Library - Part 5 - Maps
// FIRSTNAME, LASTNAME

#include <iostream>
#include <string>
#include <map>
using namespace std;

int main()
{
	map<char, string> colors;
	colors['r'] = "FF0000";
	colors['g'] = "00FF00";
	colors['b'] = "0000FF";
	colors['c'] = "00FFFF";
	colors['m'] = "FF00FF";
	colors['y'] = "FFFF00";
	bool done = false;
	while (!done) {
		char color;
		cout << "Enter a character. Enter q to leave. ";
		cin >> color;
		cout << "Hex: " << colors[color] << endl<<endl;
		if (color == 'q') {
			done = true;
		}
	}
	cout << "Goodbye"<<endl;
    cin.ignore();
    cin.get();
    return 0;
}
