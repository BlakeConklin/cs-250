// Lab - Standard Template Library - Part 2 - Lists
// Blake Conklin

#include <iostream>
#include <list>
#include <string>
using namespace std;

void DisplayList(list < string >& states)
{
	for (list < string >::iterator it = states.begin();
		it != states.end();
		it++)
	{
		cout << *it << " \t ";
	}
	cout << endl;
}
int main()
{
	list<string> states;
	bool done = false;

	while (!done) {

		cout << endl << "State list size: " <<states.size()<< endl;

		cout << "1. Add new state to front "
			<< "2. Add new state to back "
			<< "3. Remove state from front "
			<< "4. Remove state from back "
			<< "5. Continue. " << endl;
		int choice;
		cin >> choice;
		if (choice == 1) {

			string newState;

			cout << "ADD STATE TO FRONT" << endl;
			cout << "What is the name of the state? ";
			cin >> newState;

			states.push_front(newState);
		}
		else if (choice == 2) {

			string newState;

			cout << "ADD STATE TO BACK" << endl;
			cout << "What is the name of the state? ";
			cin >> newState;

			states.push_back(newState);

		}
		else if (choice == 3) {
			states.pop_front();
		}
		else if (choice == 4) {
			states.pop_back();
		}
		else if (choice == 5) {
			done = true;
		}
	}
	cout << "Normal list: "<<endl; 
	DisplayList(states);
	cout << endl;

	cout << "Reverse list: "<<endl;
	states.reverse();
	DisplayList(states);
	cout << endl;

	cout << "Sorted list: "<<endl;
	states.sort();
	DisplayList(states);
	cout << endl;

	cout << "Reverse Sorted list: "<<endl;
	states.reverse();
	DisplayList(states);
	cout << endl;

    cin.ignore();
    cin.get();
    return 0;
}

