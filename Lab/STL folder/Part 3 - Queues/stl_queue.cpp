// Lab - Standard Template Library - Part 3 - Queues
// FIRSTNAME, LASTNAME

#include <iostream>
#include <string>
#include <queue>
using namespace std;

int main()
{
	queue<float> pends;
	bool done = false;
	while (!done) {

		double transaction;

		cout << "Transactions queued: " << pends.size() << endl;

		cout << "Enter an amount to transfer or enter 0 to continue: ";
		cin >> transaction;


		if (transaction == 0) {
			done = true;
		}
		else {
			pends.push(transaction);
			cout << transaction << " pushed to account" << endl;
		}
	}
	float final=0;
	while (!pends.empty()) {

		cout << pends.front()<<endl;

		final += pends.front();
		
		pends.pop();

	}

	cout <<"Final balance: $"<< final << endl;
	cout << "Goodbye";

    cin.ignore();
    cin.get();
    return 0;
}
