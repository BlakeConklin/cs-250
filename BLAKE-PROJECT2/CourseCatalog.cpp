#include "CourseCatalog.hpp"

#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

#include "UTILITIES/Menu.hpp"
#include "EXCEPTIONS/CourseNotFoundException.hpp"
#include "DATA_STRUCTURES/Stack.hpp" // LinkedStack

CourseCatalog::CourseCatalog()
{
    LoadCourses();
}

void CourseCatalog::LoadCourses() // done
{
    Menu::Header( "LOADING COURSES" );

    ifstream input( "courses.txt" );

    if ( !input.is_open() )
    {
        cout << "Error opening input text file, courses.txt" << endl;
        return;
    }

    string label, courseCode, courseName, prerequisite;
    Course newCourse;

    while ( input >> label )
    {
        if ( label == "COURSE" )
        {
            if ( newCourse.name != "" )
            {
                m_courses.PushBack( newCourse );
                newCourse.Clear();
            }

            input >> newCourse.code >> newCourse.name;
        }
        else if ( label == "PREREQ" )
        {
            input >> newCourse.prereq;
        }
    }

    input.close();

    cout << " * " << m_courses.Size() << " courses loaded" << endl << endl;
}

void CourseCatalog::ViewCourses()
{
    Menu::Header( "VIEW COURSES" );
	cout << "#" << "\t" << "CODE \t" << "TITLE" << endl;
	cout << "\t   " << "PREREQS"<< endl;
	cout << "--------------------------------------------------------------------------------" << endl;
	for (int i = 0; i < m_courses.Size(); i++) {
		cout << i << "\t" <<m_courses[i].code<<"   "<< m_courses[i].name << endl;
		cout << "\t   "<< m_courses[i].prereq<<endl;
	}
	cout << endl;
}

Course CourseCatalog::FindCourse( const string& code )
{
	
	for (int i = 0; i < m_courses.Size(); i++) {
		if (m_courses[i].code == code) {
			return m_courses[i];
		}
	}
	throw CourseNotFound("Course not found.");
}

void CourseCatalog::ViewPrereqs()
{
	LinkedStack<Course> prereqs;
    Menu::Header( "GET PREREQS" );
	string code;
	Course current;
	cout << "Enter a Course Code: ";
	cin >> code;
	try {
		current = FindCourse(code);
	}
	catch (CourseNotFound& ex) {
		cout << ex.what() << endl;
		return;
	}
	prereqs.Push(current);
	while (current.prereq != "") {
		

		try {
			current = FindCourse(current.prereq);
		}
		catch (CourseNotFound& ex) {
			cout << ex.what() << endl;
			break;
		}
		prereqs.Push(current);
	}
	
	int i = 1;
	while (prereqs.Size()!=0) {
		
		current = prereqs.Top();
		cout << i << ".   " << current.code << "  " << current.name <<endl;
		prereqs.Pop();
		i++;
	}
}

void CourseCatalog::Run()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "MAIN MENU" );

        int choice = Menu::ShowIntMenuWithPrompt( { "View all courses", "Get course prerequisites", "Exit" } );

        switch( choice )
        {
            case 1:
                ViewCourses();
            break;

            case 2:
                ViewPrereqs();
            break;

            case 3:
                done = true;
            break;
        }
    }
}
