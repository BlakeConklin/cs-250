#ifndef _LIST_HPP
#define _LIST_HPP

const int ARRAY_SIZE = 100;
	
template <typename T>
class List
{
private:
    // private member variables
    int m_itemCount;
	T m_arr[ARRAY_SIZE];


    // functions for interal-workings
    bool ShiftRight( int atIndex )
    {
		T temp = m_arr[m_itemCount-1];

		for (int i = m_itemCount-1; i > 0; i--)
		{
			m_arr[i] = m_arr[i-1];
		}
		m_arr[0] = temp;
        return true;
    }

    bool ShiftLeft( int atIndex )
    {
		T temp = m_arr[atIndex];

		for (int i = 0; i<(m_itemCount - 1); i++)
		{
			m_arr[i] = m_arr[i + 1];
		}
		m_arr[m_itemCount - 1] = temp;

        return true;
    }

public:
    List()
    {
		m_itemCount = 0;
    }

    ~List()
    {
    }

    // Core functionality
    int     Size() const
    {
        return (m_itemCount);
    }

    bool    IsEmpty() const
    {
		return (m_itemCount == 0);
    }

    bool    IsFull() const
    {
		return (m_itemCount == ARRAY_SIZE);
    }

    bool    PushFront( const T& newItem )
    {
		if (IsFull()) {
			return false;
		}
		for (int i = m_itemCount; i>=0; i--)
		{
			m_arr[i+1] = m_arr[i];
		}
		m_arr[0] = newItem;
		m_itemCount++;
        return true;
    }

    bool    PushBack( const T& newItem )
    {
		if (IsFull()) {
			return false;
		}
		m_arr[m_itemCount] = newItem;
		m_itemCount++;
		return true;
    }

    bool    Insert( int atIndex, const T& item )
    {
		if (IsFull()) {
			return false;
		}
		for (int pos = m_itemCount; pos >= atIndex; pos--) {
			m_arr[pos + 1] = m_arr[pos];
		}
		m_arr[atIndex] = item;
		m_itemCount++;
        return true;
    }

    bool    PopFront()
    {
		ShiftLeft(0);
		m_itemCount--;
        return true;
    }

    bool    PopBack()
    {
		m_itemCount--;
        return true;
    }

    bool    Remove( const T& item )
    {
		
        return false;
    }

    bool    Remove( int atIndex )
    {
		if (m_itemCount == 0) {
			return false;
		}

		for (int pos = atIndex; pos < m_itemCount; pos++) {
			m_arr[pos] = m_arr[pos + 1];
		}
		m_itemCount--;

        return true;
    }

    void    Clear()
    {
		m_itemCount = 0;
    }

    // Accessors
    T*      Get( int atIndex )
    {
		T *arrPtr = &m_arr[atIndex];
        return arrPtr;
    }

    T*      GetFront()
    {
		T *arrPtr = &m_arr[0];
        return arrPtr;
    }

    T*      GetBack()
    {
		T *arrPtr = &m_arr[m_itemCount-1];
		return arrPtr;
    }

    // Additional functionality
    int     GetCountOf( const T& item ) const
    {
		int count = 0;
		for (int i = 0; i < m_itemCount; i++) {
			if (m_arr[i] == item) {
				count++;
			}
		}
        return count; // placeholder
    }
    bool    Contains( const T& item ) const
    {
		for (int i = 0; i < m_itemCount; i++) {
			if (m_arr[i] == item) {
				return true;
			}
		}
		return false;
    }

    friend class Tester;
};


#endif
