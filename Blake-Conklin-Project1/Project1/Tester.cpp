#include "Tester.hpp"

void Tester::RunTests()
{
    Test_IsEmpty();
    Test_IsFull();
    Test_Size();
    Test_GetCountOf();
    Test_Contains();

    Test_PushFront();
    Test_PushBack();

    Test_Get();
    Test_GetFront();
    Test_GetBack();

    Test_PopFront();
    Test_PopBack();
    Test_Clear();

    Test_ShiftRight();
    Test_ShiftLeft();

    Test_Remove();
    Test_Insert();
}

void Tester::DrawLine()
{
    cout << endl;
    for ( int i = 0; i < 80; i++ )
    {
        cout << "-";
    }
    cout << endl;
}


void Tester::Test_ShiftRight()
{
    DrawLine();
    cout << "TEST: Test_ShiftRight" << endl << endl;

    // Put tests here
	{
		cout << "Test 1" << endl;
		List<string>testList;

		testList.PushFront("A");
		testList.PushFront("B");
		testList.PushFront("C");
		testList.ShiftRight(0);
		string expectedValue = "A";
		string actualValue = testList.m_arr[0];
		cout << "Add three char items and shift them right. element 0 should be A." << endl;
		if (actualValue == expectedValue)
		{
			cout << "Pass" << endl << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{
		//Test2
		cout << "Test 2" << endl;
		List<string>testList;

		testList.PushFront("A");
		testList.PushFront("B");
		testList.PushFront("C");
		testList.ShiftRight(0);
		string expectedValue = "C";
		string actualValue = testList.m_arr[1];
		cout << "Add three char items and shift them right. element 1 should be C." << endl;
		if (actualValue == expectedValue)
		{
			cout << "Pass" << endl << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
}

void Tester::Test_ShiftLeft()
{
    DrawLine();
    cout << "TEST: Test_ShiftLeft" << endl << endl;

    // Put tests here
	{
		cout << "Test 1" << endl;
		List<string>testList;

		testList.PushFront("A");
		testList.PushFront("B");
		testList.PushFront("C");
		testList.ShiftLeft(0);
		string expectedValue = "A";
		string actualValue = testList.m_arr[1];
		cout << "Add three char items and shift them left. element 1 should be A." << endl;
		if (actualValue == expectedValue)
		{
			cout << "Pass" << endl << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{
		cout << "Test 2" << endl;
		List<string>testList;

		testList.PushFront("A");
		testList.PushFront("B");
		testList.PushFront("C");
		testList.ShiftLeft(0);
		string expectedValue = "B";
		string actualValue = testList.m_arr[0];
		cout << "Add three char items and shift them left. element 0 should be B." << endl;
		if (actualValue == expectedValue)
		{
			cout << "Pass" << endl << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{
		cout << "Test 3" << endl;
		List<string>testList;

		testList.PushFront("A");
		testList.PushFront("B");
		testList.PushFront("C");
		testList.ShiftLeft(0);
		string expectedValue = "C";
		string actualValue = testList.m_arr[2];
		cout << "Add three char items and shift them left. element 2 should be C." << endl;
		if (actualValue == expectedValue)
		{
			cout << "Pass" << endl << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
}

void Tester::Test_Size()
{
    DrawLine();
    cout << "TEST: Test_Size" << endl << endl;

    {   // Test begin
        cout << "Test 1" << endl;
        List<int> testList;

		testList.m_itemCount = 0;
        int expectedSize = 0;
        int actualSize = testList.Size();

        cout << "Expected size: " << expectedSize << endl;
        cout << "Actual size:   " << actualSize << endl;

        if ( actualSize == expectedSize )
        {
            cout << "Pass" << endl << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
    }   // Test end

    {   // Test begin
        cout << "Test 2" << endl;
        List<int> testList;

		testList.m_itemCount = 0;
        testList.PushBack( 1 );

        int expectedSize = 1;
        int actualSize = testList.Size();

        cout << "Expected size: " << expectedSize << endl;
        cout << "Actual size:   " << actualSize << endl;

        if ( actualSize == expectedSize )
        {
            cout << "Pass" << endl << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
    }   
	{   // Test begin
		cout << "Test 3" << endl;
		List<int> testList;

		testList.m_itemCount = 0;
		testList.PushBack(1);
		for (int i = 0; i <= ARRAY_SIZE; i++) {
			testList.PushBack(1);
		}

		int expectedSize = 100;
		int actualSize = testList.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Pass" << endl << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	
}

void Tester::Test_IsEmpty()
{
    DrawLine();
    cout << "TEST: Test_IsEmpty" << endl << endl;

    // Put tests here
	{
		cout << "Test 1" << endl;
		List<int> testlist;

		testlist.m_itemCount = 0;
		bool expectedValue = true;
		bool actualValue = testlist.IsEmpty();
		cout << "Create a list and check if empty. Should return true." << endl;
		if (expectedValue == actualValue) {
			cout << "Pass" << endl << endl;
		}
		else
			cout << "Fail" << endl;
	}
	{
		
		cout << "Test 2" << endl;
		List<int> testlist;

		testlist.m_itemCount = 0;
		bool expectedValue = true;
		testlist.PushBack(1);
		testlist.PopFront();
		bool actualValue = testlist.IsEmpty();
		cout << "Create a list and add an item then pop that item and finally check if empty. Should return true." << endl;
		if (expectedValue == actualValue) {
			cout << "Pass" << endl << endl;
		}
		else
			cout << "Fail";
	
	}
}

void Tester::Test_IsFull()
{
    DrawLine();
    cout << "TEST: Test_IsFull" << endl << endl;
    // Put tests here
	{
		cout << endl << "Test 1" << endl;
		List<string> testlist;
		testlist.m_itemCount = 0;

		bool expectedValue = false;
		bool actualValue = testlist.IsFull();
		cout << "Create a list, Should return Pass." << endl;
		if (expectedValue == actualValue) {
			cout << "Pass" << endl << endl;
		}
		else
			cout << "Fail" << endl;
	}
	{
		//test 2
		cout << endl << "Test 2" << endl;
		List<string> testlist;
		testlist.m_itemCount = 0;
		bool expectedValue = true;
		for (int i = 0; i <= ARRAY_SIZE; i++) {
			testlist.PushBack("a");
		}
		bool actualValue = testlist.IsFull();
		
		cout << "Create a list that is full, Should return pass"<<endl;
		if (expectedValue == actualValue) {
			cout << "Pass" << endl << endl;
		}
		else
			cout << "Fail" << endl;
	
	}
}

void Tester::Test_PushFront()
{
    DrawLine();
    cout << "TEST: Test_PushFront" << endl << endl;
	
	{
		// Put tests here
		cout << "Test 1" << endl;
		List<string> testlist;

		testlist.PushFront("A");
		string expectedValue = "A";
		string actualValue = *testlist.Get(0);
		cout << "Create a list, Pushfront 1 item, should be successful" << endl;
		if (expectedValue == actualValue) {
			cout << "Pass" << endl << endl;
		}
		else
			cout << "Fail" << endl;
	}
	{
		// Put tests here
		List<string> testlist;

		cout << "Test 2" << endl;
		cout << "Pushfront three items and check to make sure there is three items in the array." << endl;
		testlist.PushFront("A");
		testlist.PushFront("B");
		testlist.PushFront("C");
		int expectedValue = 3;
		int actualValue = testlist.Size();
		if (expectedValue == actualValue) {
			cout << "Pass" << endl << endl;
		}
		else
			cout << "Fail" << endl;
	}
}


void Tester::Test_PushBack()
{
    DrawLine();
    cout << "TEST: Test_PushBack" << endl << endl;

    // Put tests here
	{
		cout << "Test1" << endl;
		List<string> testlist;
		testlist.PushBack("A");
		string expectedValue = "A";
		string actualValue = *testlist.Get(0);

		cout << "Create a list, PushBack 1 item, should be successful" << endl;
		if (expectedValue == actualValue) {
			cout << "Pass" << endl << endl;
		}
		else
			cout << "Fail" << endl;
	}
	{
		//Test 2
		cout << "Test 2" << endl;

		List<string> testlist;
		testlist.PushBack("B");
		testlist.PushBack("A");
		string expectedValue = "A";
		string actualValue = *testlist.Get(1);

		cout << "Create a list, PushBack 2 item, should be A" << endl;

		if (expectedValue == actualValue) {
			cout << "Pass" << endl << endl;
		}
		else
			cout << "Fail" << endl;
	}
}

void Tester::Test_PopFront()
{
    DrawLine();
    cout << "TEST: Test_PopFront" << endl << endl;
	{
		cout << "Test 1" << endl;
		List<string> testlist;

		testlist.PushFront("1");
		testlist.PopFront();
		bool expectedvalue = true;
		bool actualvalue = testlist.IsEmpty();
		cout << "adds an item then popfront that item. Should return successful." << endl;
		if (expectedvalue == actualvalue) {
			cout << "Pass" << endl<<endl;
		}
		else
			cout << "Fail" << endl;
	}
	{
		cout << "Test 2" << endl;
		List<int> testlist;

		testlist.PushFront(1);
		testlist.PushFront(2);
		testlist.PushFront(3);
		testlist.PopFront();
		int expectedvalue = 2;
		int actualvalue = *testlist.GetFront();
		cout << "Adds three items then Gets the front. Should return 2." << endl;
		if (expectedvalue == actualvalue) {
			cout << "Pass" << endl << endl;
		}
		else
			cout << "Fail" << endl;
	}
}

void Tester::Test_PopBack()
{
    DrawLine();
    cout << "TEST: Test_PopBack" << endl << endl;
	{
		// Put tests here
		cout << "Test 1" << endl;
		List<string> testlist;

		testlist.PushBack("1");
		testlist.PopBack();
		bool expectedvalue = true;
		bool actualvalue = testlist.IsEmpty();
		cout << "adds an item then pops that item from the back and checks if empty. Should return successful." << endl;
		if (expectedvalue == actualvalue) {
			cout << "Pass" << endl << endl;
		}
		else
			cout << "Fail" << endl;
	}
	{
		cout << "Test 2" << endl;
		List<string> testlist;

		testlist.PushBack("1");
		testlist.PushBack("2");
		testlist.PushBack("3");
		testlist.PushBack("4");
		testlist.PopBack();
		string expectedvalue = "123";
		string actualvalue;
		for (int i = 0; i < testlist.Size(); i++) {
			actualvalue += testlist.m_arr[i];
		}
		cout << "adds an item then pops that item from the back. Should return successful." << endl;
		if (expectedvalue == actualvalue) {
			cout << "Pass" << endl << endl;
		}
		else
			cout << "Fail" << endl;
	}
}

void Tester::Test_Clear()
{
	DrawLine();
	cout << "TEST: Test_Clear" << endl << endl;

	// Put tests here
	
	{
		cout << "Test 1" << endl;
		List<int> testlist;

		testlist.PushFront(1);
		testlist.PushFront(2);
		testlist.PushBack(5);
		testlist.Clear();
		cout << "add three items then call clear. Should return empty." << endl;
		if (testlist.IsEmpty() == true) {
			cout << "Pass" << endl << endl;
		}
		else
			cout << "Fail" << endl;
	}
	{
		cout << "Test 2" << endl;
		List<int> testlist;

		testlist.Clear();
		cout << "Use clear without any inputs. Should return empty." << endl;
		if (testlist.IsEmpty() == true) {
			cout << "Pass" << endl << endl;
		}
		else
			cout << "Fail" << endl;
	}
}

void Tester::Test_Get()
{
    DrawLine();
    cout << "TEST: Test_Get" << endl << endl;

    // Put tests here
	{
		cout << "Test 1" << endl;
		List<int> testlist;

		testlist.PushFront(1);
		testlist.PushFront(2);
		testlist.PushBack(5);
		int expectedvalue = 1;

		cout << "adds three items then finds element 1 is 1. Should return true." << endl;

		if (expectedvalue == *testlist.Get(1)) {

			cout << "Pass" << endl << endl;
		}
		else {
			cout << *testlist.Get(1) << endl;
			cout << "Fail" << endl;
		}
	}
	{
		cout << "Test 2" << endl;
		List<int> testlist;

		testlist.PushFront(1);
		testlist.PushFront(2);
		testlist.PushBack(5);
		int expectedvalue = 2;

		cout << "adds three items then finds element 0 is 2. Should return true." << endl;

		if (expectedvalue == *testlist.Get(0)) {

			cout << "Pass" << endl << endl;
		}
		else {
			cout << *testlist.Get(1) << endl;
			cout << "Fail" << endl;
		}
	}
}

void Tester::Test_GetFront()
{
    DrawLine();
    cout << "TEST: Test_GetFront" << endl << endl;

    // Put tests here
	
	{
		cout << "Test 1" << endl;
		List<int> testlist;

		testlist.PushFront(1);
		testlist.PushFront(2);
		int expectedvalue = 2;

		cout << "adds two items then finds what the front is 2. Should return true." << endl;

		if (expectedvalue == *testlist.GetFront()) {

			cout << "Pass" << endl << endl;
		}
		else {
			cout << *testlist.GetFront() << endl;
			cout << "Fail" << endl;
		}
	}
	{
		cout << "Test 2" << endl;
		List<int> testlist;
		
		testlist.PushFront(1);
		testlist.PushFront(2);
		testlist.PushFront(7);
		testlist.PushFront(5);
		int expectedvalue = 5;

		cout << "adds two items then finds the front is 5. Should return true." << endl;

		if (expectedvalue == *testlist.GetFront()) {

			cout << "Pass" << endl << endl;
		}
		else {
			cout << *testlist.GetFront() << endl;
			cout << "Fail" << endl;
		}
	}
}

void Tester::Test_GetBack()
{
    DrawLine();
    cout << "TEST: Test_GetBack" << endl << endl;

    // Put tests here
	{
		cout << "Test 1" << endl;
		List<int> testlist;

		testlist.PushFront(1);
		testlist.PushFront(2);
		int expectedvalue = 1;

		cout << "Adds two items then finds the back is 1. Should return true." << endl;
		if (expectedvalue == *testlist.GetBack()) {
			cout << "Pass" << endl << endl;
		}
		else
			cout << "Fail" << endl;
	}
	{
		cout << "Test 2" << endl;
		List<int> testlist;

		testlist.PushFront(1);
		testlist.PushFront(2);
		testlist.PushBack(5);
		testlist.PushFront(2);
		testlist.PushBack(3);
		int expectedvalue = 3;

		cout << "Adds five items then finds the back is 3. Should return true." << endl;
		if (expectedvalue == *testlist.GetBack()) {
			cout << "Pass" << endl << endl;
		}
		else
			cout << "Fail" << endl;
	}
}

void Tester::Test_GetCountOf()
{
    DrawLine();
    cout << "TEST: Test_GetCountOf" << endl << endl;

    // Put tests here
	{
		cout << "Test 1" << endl;
		List<int> testlist;

		testlist.PushFront(1);
		testlist.PushFront(2);
		testlist.PushFront(2);
		testlist.PushFront(2);
		testlist.PushBack(5);
		testlist.PushBack(10);
		int expectedvalue = 3;

		cout << "add 6 items, count the number of '2's and there should be a total of 3." << endl;
		if (expectedvalue == testlist.GetCountOf(2)) {
			cout << "Pass" << endl << endl;
		}
		else
			cout << "Fail" << endl;
	}
	{
		cout << "Test 2" << endl;
		List<int> testlist;

		testlist.PushFront(1);
		testlist.PushFront(5);
		testlist.PushBack(5);
		testlist.PushBack(10);
		int expectedvalue = 2;

		cout << "add 4 items, count the number of '5's and there should be a total of 2." << endl;
		if (expectedvalue == testlist.GetCountOf(5)) {
			cout << "Pass" << endl << endl;
		}
		else
			cout << "Fail" << endl;
	}
	{
		cout << "Test 2" << endl;
		List<int> testlist;

		testlist.PushFront(1);
		testlist.PushFront(5);
		testlist.PushBack(5);
		testlist.PushBack(10);
		int expectedvalue = 0;

		cout << "add 4 items, count the number of '5's and there should be a total of 2." << endl;
		if (expectedvalue == testlist.GetCountOf(4)) {
			cout << "Pass" << endl << endl;
		}
		else
			cout << "Fail" << endl;
	}
}

void Tester::Test_Contains()
{
	DrawLine();
	cout << "TEST: Test_Contains" << endl << endl;

	// Put tests here
	{
		List<int> testlist;
		cout << "Test 1" << endl;
		testlist.PushFront(1);
		testlist.PushFront(2);
		testlist.PushBack(5);
		testlist.PushBack(10);

		cout << "add four items check if one is 5, should return true." << endl;
		if (testlist.Contains(5) == true) {
			cout << "Pass" << endl << endl;
		}
		else
			cout << "Fail" << endl;
	}
	{
		List<int> testlist;
		cout << "Test 2" << endl;
		testlist.PushFront(1);
		testlist.PushFront(2);
		testlist.PushBack(5);
		testlist.PushBack(2);

		cout << "add four items check if one is 2, should return true." << endl;
		if (testlist.Contains(2) == true) {
			cout << "Pass" << endl << endl;
		}
		else
			cout << "Fail" << endl;
	}
	{
		List<int> testlist;
		cout << "Test 3" << endl;
		testlist.PushFront(1);
		testlist.PushFront(2);
		testlist.PushBack(5);
		testlist.PushBack(2);

		cout << "add four items check if one is 4, should return false." << endl;
		if (testlist.Contains(4) == false) {
			cout << "Pass" << endl << endl;
		}
		else
			cout << "Fail" << endl;
	}
}

void Tester::Test_Remove()
{
    DrawLine();
    cout << "TEST: Test_Remove" << endl << endl;

    // Put tests here
	{
		cout<< "Test 1" << endl;

		List<string> testlist;
		testlist.PushBack("B");
		testlist.Remove(1);

		int expectedValue = 0;
		int actualValue = testlist.Size();

		cout << "Create a list, add 1 item then remove it, should return no items" << endl;

		if (expectedValue == actualValue) {
			cout << "Pass" << endl << endl;
		}
		else
			cout << "Fail" << endl;
	}
	{
		cout << "Test 2" << endl;

		List<string> testlist;
		testlist.PushBack("B");
		testlist.PushBack("B");
		testlist.PushFront("B");
		testlist.Remove(1);

		int expectedValue = 2;
		int actualValue = testlist.Size();

		cout << "Create a list, add three items then remove one, should return two items." << endl;

		if (expectedValue == actualValue) {
			cout << "Pass" << endl << endl;
		}
		else
			cout << "Fail" << endl;
	}
}

void Tester::Test_Insert()
{
	DrawLine();
	cout << "TEST: Test_Insert" << endl << endl;

	// Put tests here
	cout << "Test 1" << endl;
	{
		List<string> testlist;
		testlist.Insert(1, "B");

		int expectedValue = 1;
		int actualValue = testlist.Size();

		cout << "Create a list, insert 1 item, should return item" << endl;

		if (expectedValue == actualValue) {
			cout << "Pass" << endl << endl;
		}
		else
			cout << "Fail" << endl;
	}
	{
		List<string> testlist;
		cout << "Test 2" << endl;
		testlist.Insert(0, "A");
		testlist.Insert(1, "B");
		testlist.Insert(2, "C");

		string expectedValue = "C";
		string actualValue = *testlist.GetBack();

		cout << "Create a list, insert three items, should return the back item." << endl;

		if (expectedValue == actualValue) {
			cout << "Pass" << endl << endl;
		}
		else
			cout << "Fail" << endl;
	}
}

