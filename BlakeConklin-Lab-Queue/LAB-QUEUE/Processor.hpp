#ifndef _PROCESSOR
#define _PROCESSOR

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
using namespace std;

#include "Job.hpp"
#include "Queue.hpp"

class Processor
{
    public:
    void FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile );
    void RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile );
};

void Processor::FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile )
{
	ofstream output(logFile);
	output << "First Come First Served (FCFS)" << endl;
	
	int counter = 0;
	int jobcounter = 0;
	while (jobQueue.Size()>0) {
		if (jobcounter == 0) 
		{
			output << "\t \t Processing Job #: " << jobQueue.Front()->id << endl;
		}

		jobQueue.Front()->Work(FCFS);
		output << "Current Cycle: " << counter<< "\t \t REMAINING: " << jobQueue.Front()->fcfs_timeRemaining << endl;
		jobcounter++;
		if (jobQueue.Front()->fcfs_done) {

			jobQueue.Front()->fcfs_finishTime=counter;
			jobQueue.Pop();
			jobcounter = 0;
		}
		
		counter++;
		
	}
	double avg=0;
	double total = 0;

	for (int i = 0; i < allJobs.size(); i++) {
		output << "Summary of Job ID: " << allJobs[i].id<<"\t \t Job Finish Time: "<<allJobs[i].fcfs_finishTime<<endl;
		avg += allJobs[i].fcfs_finishTime;
		total++;
	}
	avg = avg / total;
	output << "Total time: " << counter<<endl;
	output << "Average time of jobs: " << avg << endl;
	output.close();
}

void Processor::RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile )
{
	ofstream output(logFile);
	output << "Round Robin (RR)" << endl;
	int counter = 0;
	int timer = 0;
	while (jobQueue.Size() > 0) {		
		if (timer == timePerProcess) 
		{
			jobQueue.Front()->rr_timesInterrupted + 1;
			jobQueue.Push(jobQueue.Front());
			jobQueue.Pop();
			timer = 0;
		}

		if (timer == 0)
		{
			output << "\t \t Processing Job #: " << jobQueue.Front()->id << endl;
		}

		jobQueue.Front()->Work(RR);
		output << "Current Cycle: " << counter << "\t \t REMAINING: " << jobQueue.Front()->rr_timeRemaining << endl;
		if (jobQueue.Front()->rr_done) {

			jobQueue.Front()->rr_finishTime = counter;
			jobQueue.Pop();
		}
		counter++;
		timer++;
	}
	double avg = 0;
	double total = 0;
	for (int i = 0; i < allJobs.size(); i++) {
		output << "Summary of Job ID: " << allJobs[i].id << "\t \t Job Finish Time: " << allJobs[i].rr_finishTime << endl;
		avg += allJobs[i].rr_finishTime;
		total++;
	}
	avg = avg / total;
	output << "Total time: " << counter<<endl;
	output << "Average time of jobs: " << avg << endl;
	output << "round robin interval: " << timePerProcess << endl;
	output.close();
}

#endif
