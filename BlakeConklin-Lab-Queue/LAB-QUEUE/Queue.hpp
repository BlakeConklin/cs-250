#ifndef QUEUE_HPP
#define QUEUE_HPP

#include "LinkedList.hpp"

template <typename T>
class Queue
{
    public:
    void Push( T data )
    {
        //throw runtime_error( "not yet implemented" ); // placeholder
		m_list.PushBack(data);
    }

    void Pop()
    {
        //throw runtime_error( "not yet implemented" ); // placeholder
		m_list.PopFront();
    }

    T& Front()
    {
        //throw runtime_error( "not yet implemented" ); // placeholder
		return m_list.GetFirst();
    }

    int Size()
    {
        //throw runtime_error( "not yet implemented" ); // placeholder
		return m_list.Size();
    }

    private:
		LinkedList<T> m_list;
};

#endif
